-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10 Jan 2018 pada 00.54
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `knn`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `knn`
--

CREATE TABLE `knn` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `ipsem1` float NOT NULL,
  `ipsem2` float NOT NULL,
  `ipsem3` float NOT NULL,
  `ipsem4` float NOT NULL,
  `ipsem5` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `knn`
--

INSERT INTO `knn` (`id`, `nama`, `ipsem1`, `ipsem2`, `ipsem3`, `ipsem4`, `ipsem5`) VALUES
(1, 'gendut', 3.5, 3.2, 3.9, 3.8, 3.7),
(2, 'endut', 3.5, 3.7, 3.6, 3.5, 3.9),
(3, 'bagas', 3.5, 3.7, 3.2, 3.5, 3.1),
(4, 'bagus', 3.7, 3.8, 3.9, 3.2, 3.5),
(5, 'agus', 3.6, 2.2, 2.7, 2.9, 3.1),
(6, 'agas', 3.5, 2.5, 2.9, 3.1, 3.5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `knn`
--
ALTER TABLE `knn`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `knn`
--
ALTER TABLE `knn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
