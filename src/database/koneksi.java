/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Harun Ar
 */
public class koneksi {
private static Connection koneksi;

public static Connection koneksiDatabase(){
	if(koneksi==null){
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setUrl("jdbc:mysql://localhost/knn");
		dataSource.setUser("root");
		dataSource.setPassword("");
		try{
			koneksi = (Connection) dataSource.getConnection();
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, "Error Koneksi :" + e.getMessage());
		}
	}
	return koneksi;
}
    
}
