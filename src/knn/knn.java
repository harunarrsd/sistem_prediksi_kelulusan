/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knn;

import database.koneksi;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import jxl.Sheet;
import jxl.Workbook;

/**
 *private void muatData() {
    File excelFile = new File("Anggota.xls");
 
    // buat model untuk file excel
    if (excelFile.exists()) {
        try {
            Workbook workbook = Workbook.getWorkbook(excelFile);
            Sheet sheet = workbook.getSheets()[0];
 
            TableModel model = new DefaultTableModel(sheet.getRows(), sheet.getColumns());
            for (int row = 0; row < sheet.getRows(); row++) {
                for (int column = 0; column < sheet.getColumns(); column++) {
                    String content = sheet.getCell(column, row).getContents();
                    model.setValueAt(content, row, column);
                }
            }
 
            table.setModel(model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
        }
 
    } else {
        JOptionPane.showMessageDialog(null, "File does not exist");
    }
}private void muatData() {
    File excelFile = new File("Anggota.xls");
 
    // buat model untuk file excel
    if (excelFile.exists()) {
        try {
            Workbook workbook = Workbook.getWorkbook(excelFile);
            Sheet sheet = workbook.getSheets()[0];
 
            TableModel model = new DefaultTableModel(sheet.getRows(), sheet.getColumns());
            for (int row = 0; row < sheet.getRows(); row++) {
                for (int column = 0; column < sheet.getColumns(); column++) {
                    String content = sheet.getCell(column, row).getContents();
                    model.setValueAt(content, row, column);
                }
            }
 
            table.setModel(model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
        }
 
    } else {
        JOptionPane.showMessageDialog(null, "File does not exist");
    }
}private void muatData() {
    File excelFile = new File("Anggota.xls");
 
    // buat model untuk file excel
    if (excelFile.exists()) {
        try {
            Workbook workbook = Workbook.getWorkbook(excelFile);
            Sheet sheet = workbook.getSheets()[0];
 
            TableModel model = new DefaultTableModel(sheet.getRows(), sheet.getColumns());
            for (int row = 0; row < sheet.getRows(); row++) {
                for (int column = 0; column < sheet.getColumns(); column++) {
                    String content = sheet.getCell(column, row).getContents();
                    model.setValueAt(content, row, column);
                }
            }
 
            table.setModel(model);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
        }
 
    } else {
        JOptionPane.showMessageDialog(null, "File does not exist");
    }
}
 * @author Harun Ar
 */
public class knn extends javax.swing.JFrame {

    /**
     * Creates new form knn
     */
    public knn() {
        initComponents();
        isiTable();
        isiTable2();
        
    }
    
    public void isiTable(){
	DefaultTableModel dtm = (DefaultTableModel)table.getModel();
	String query = "select * from knn";
	try{
		Connection c = koneksi.koneksiDatabase();
		Statement s = c.createStatement();
		ResultSet r = s.executeQuery(query);
		while(r.next()){
			String id = r.getString("id");
			String nama = r.getString("nama");
			String ipsem1 = r.getString("ipsem1");
			String ipsem2 = r.getString("ipsem2");
			String ipsem3 = r.getString("ipsem3");
			String ipsem4 = r.getString("ipsem4");
                        String ipsem5 = r.getString("ipsem5");
			Object[] a = new Object[] {id, nama, ipsem1, ipsem2, ipsem3, ipsem4, ipsem5};
			dtm.addRow(a);
		}
	}catch(SQLException e){
		JOptionPane.showMessageDialog(this, e.getMessage());
	}
}
    
    public void isiTable2(){
	DefaultTableModel dtm = (DefaultTableModel)table2.getModel();
	String query = "select * from knn";
	try{
		Connection c = koneksi.koneksiDatabase();
		Statement s = c.createStatement();
		ResultSet r = s.executeQuery(query);
		while(r.next()){
			String id = r.getString("id");
			String nama = r.getString("nama");
			String ipsem1 = r.getString("ipsem1");
			String ipsem2 = r.getString("ipsem2");
			String ipsem3 = r.getString("ipsem3");
			String ipsem4 = r.getString("ipsem4");
                        String ipsem5 = r.getString("ipsem5");
			Object[] a = new Object[] {id, nama, ipsem1, ipsem2, ipsem3, ipsem4, ipsem5};
			dtm.addRow(a);
		}
	}catch(SQLException e){
		JOptionPane.showMessageDialog(this, e.getMessage());
	}
}
    
    public void rumus(){
        float a1 = Float.valueOf(this.Txtsem6.getText()).floatValue();
        float a2 = Float.valueOf(this.Txtsem1.getText()).floatValue();
        float b1 = Float.valueOf(this.Txtsem7.getText()).floatValue();
        float b2 = Float.valueOf(this.Txtsem2.getText()).floatValue();
        float c1 = Float.valueOf(this.Txtsem8.getText()).floatValue();
        float c2 = Float.valueOf(this.Txtsem3.getText()).floatValue();
        float d1 = Float.valueOf(this.Txtsem9.getText()).floatValue();
        float d2 = Float.valueOf(this.Txtsem4.getText()).floatValue();
        float e1 = Float.valueOf(this.Txtsem10.getText()).floatValue();
        float e2 = Float.valueOf(this.Txtsem5.getText()).floatValue();
        float pengurangan1 = a1-a2;
        float pengurangan2 = b1-b2;
        float pengurangan3 = c1-c2;
        float pengurangan4 = d1-d2;
        float pengurangan5 = e1-e2;
        float pangkat1 = (float) Math.pow(pengurangan1, 2);
        float pangkat2 = (float) Math.pow(pengurangan2, 2);
        float pangkat3 = (float) Math.pow(pengurangan3, 2);
        float pangkat4 = (float) Math.pow(pengurangan4, 2);
        float pangkat5 = (float) Math.pow(pengurangan5, 2);
        float penjumlahan = pangkat1+pangkat2+pangkat3+pangkat4+pangkat5;
        float akar = (float) Math.sqrt(penjumlahan);
        
        TxtDistance.setText(String.valueOf(akar));
    }
    
    public void rumus2(){
        float a1 = Float.valueOf(this.Txtsem6.getText()).floatValue();
        float a2 = Float.valueOf(this.Txtsem1.getText()).floatValue();
        float b1 = Float.valueOf(this.Txtsem7.getText()).floatValue();
        float b2 = Float.valueOf(this.Txtsem2.getText()).floatValue();
        float c1 = Float.valueOf(this.Txtsem8.getText()).floatValue();
        float c2 = Float.valueOf(this.Txtsem3.getText()).floatValue();
        float d1 = Float.valueOf(this.Txtsem9.getText()).floatValue();
        float d2 = Float.valueOf(this.Txtsem4.getText()).floatValue();
        float e1 = Float.valueOf(this.Txtsem10.getText()).floatValue();
        float e2 = Float.valueOf(this.Txtsem5.getText()).floatValue();
        float pengurangan1 = a1-a2;
        float pengurangan2 = b1-b2;
        float pengurangan3 = c1-c2;
        float pengurangan4 = d1-d2;
        float pengurangan5 = e1-e2;
        float pangkat1 = (float) Math.pow(pengurangan1, 2);
        float pangkat2 = (float) Math.pow(pengurangan2, 2);
        float pangkat3 = (float) Math.pow(pengurangan3, 2);
        float pangkat4 = (float) Math.pow(pengurangan4, 2);
        float pangkat5 = (float) Math.pow(pengurangan5, 2);
        float penjumlahan = pangkat1+pangkat2+pangkat3+pangkat4+pangkat5;
        float akar = (float) Math.sqrt(penjumlahan);
        
        TxtDistance2.setText(String.valueOf(akar));
    }
    
    public void rumus3(){
        float a1 = Float.valueOf(this.Txtsem6.getText()).floatValue();
        float a2 = Float.valueOf(this.Txtsem1.getText()).floatValue();
        float b1 = Float.valueOf(this.Txtsem7.getText()).floatValue();
        float b2 = Float.valueOf(this.Txtsem2.getText()).floatValue();
        float c1 = Float.valueOf(this.Txtsem8.getText()).floatValue();
        float c2 = Float.valueOf(this.Txtsem3.getText()).floatValue();
        float d1 = Float.valueOf(this.Txtsem9.getText()).floatValue();
        float d2 = Float.valueOf(this.Txtsem4.getText()).floatValue();
        float e1 = Float.valueOf(this.Txtsem10.getText()).floatValue();
        float e2 = Float.valueOf(this.Txtsem5.getText()).floatValue();
        float pengurangan1 = a1-a2;
        float pengurangan2 = b1-b2;
        float pengurangan3 = c1-c2;
        float pengurangan4 = d1-d2;
        float pengurangan5 = e1-e2;
        float pangkat1 = (float) Math.pow(pengurangan1, 2);
        float pangkat2 = (float) Math.pow(pengurangan2, 2);
        float pangkat3 = (float) Math.pow(pengurangan3, 2);
        float pangkat4 = (float) Math.pow(pengurangan4, 2);
        float pangkat5 = (float) Math.pow(pengurangan5, 2);
        float penjumlahan = pangkat1+pangkat2+pangkat3+pangkat4+pangkat5;
        float akar = (float) Math.sqrt(penjumlahan);
        
        TxtDistance3.setText(String.valueOf(akar));
    }
    
    public void rumus4(){
        float a1 = Float.valueOf(this.Txtsem6.getText()).floatValue();
        float a2 = Float.valueOf(this.Txtsem1.getText()).floatValue();
        float b1 = Float.valueOf(this.Txtsem7.getText()).floatValue();
        float b2 = Float.valueOf(this.Txtsem2.getText()).floatValue();
        float c1 = Float.valueOf(this.Txtsem8.getText()).floatValue();
        float c2 = Float.valueOf(this.Txtsem3.getText()).floatValue();
        float d1 = Float.valueOf(this.Txtsem9.getText()).floatValue();
        float d2 = Float.valueOf(this.Txtsem4.getText()).floatValue();
        float e1 = Float.valueOf(this.Txtsem10.getText()).floatValue();
        float e2 = Float.valueOf(this.Txtsem5.getText()).floatValue();
        float pengurangan1 = a1-a2;
        float pengurangan2 = b1-b2;
        float pengurangan3 = c1-c2;
        float pengurangan4 = d1-d2;
        float pengurangan5 = e1-e2;
        float pangkat1 = (float) Math.pow(pengurangan1, 2);
        float pangkat2 = (float) Math.pow(pengurangan2, 2);
        float pangkat3 = (float) Math.pow(pengurangan3, 2);
        float pangkat4 = (float) Math.pow(pengurangan4, 2);
        float pangkat5 = (float) Math.pow(pengurangan5, 2);
        float penjumlahan = pangkat1+pangkat2+pangkat3+pangkat4+pangkat5;
        float akar = (float) Math.sqrt(penjumlahan);
        
        TxtDistance4.setText(String.valueOf(akar));
    }
    
    public void rumus5(){
        float a1 = Float.valueOf(this.Txtsem6.getText()).floatValue();
        float a2 = Float.valueOf(this.Txtsem1.getText()).floatValue();
        float b1 = Float.valueOf(this.Txtsem7.getText()).floatValue();
        float b2 = Float.valueOf(this.Txtsem2.getText()).floatValue();
        float c1 = Float.valueOf(this.Txtsem8.getText()).floatValue();
        float c2 = Float.valueOf(this.Txtsem3.getText()).floatValue();
        float d1 = Float.valueOf(this.Txtsem9.getText()).floatValue();
        float d2 = Float.valueOf(this.Txtsem4.getText()).floatValue();
        float e1 = Float.valueOf(this.Txtsem10.getText()).floatValue();
        float e2 = Float.valueOf(this.Txtsem5.getText()).floatValue();
        float pengurangan1 = a1-a2;
        float pengurangan2 = b1-b2;
        float pengurangan3 = c1-c2;
        float pengurangan4 = d1-d2;
        float pengurangan5 = e1-e2;
        float pangkat1 = (float) Math.pow(pengurangan1, 2);
        float pangkat2 = (float) Math.pow(pengurangan2, 2);
        float pangkat3 = (float) Math.pow(pengurangan3, 2);
        float pangkat4 = (float) Math.pow(pengurangan4, 2);
        float pangkat5 = (float) Math.pow(pengurangan5, 2);
        float penjumlahan = pangkat1+pangkat2+pangkat3+pangkat4+pangkat5;
        float akar = (float) Math.sqrt(penjumlahan);
        
        TxtDistance5.setText(String.valueOf(akar));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        Txtsem1 = new javax.swing.JTextField();
        TxtNama = new javax.swing.JTextField();
        Txtsem2 = new javax.swing.JTextField();
        Txtsem3 = new javax.swing.JTextField();
        Txtsem4 = new javax.swing.JTextField();
        Txtsem5 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table2 = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        TxtNama2 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        Txtsem6 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        Txtsem7 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        Txtsem8 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        Txtsem9 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        Txtsem10 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        TxtDistance = new javax.swing.JTextField();
        BtnProses1 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        TxtDistance2 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        TxtDistance3 = new javax.swing.JTextField();
        TxtDistance4 = new javax.swing.JTextField();
        TxtDistance5 = new javax.swing.JTextField();
        BtnProses2 = new javax.swing.JButton();
        BtnProses3 = new javax.swing.JButton();
        BtnProses4 = new javax.swing.JButton();
        BtnProses5 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Nama", "IPSEM1", "IPSEM2", "IPSEM3", "IPSEM4", "IPSEM5"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(table);

        jLabel2.setText("NAMA");

        jLabel3.setText("IPK SEM 2");

        jLabel4.setText("IPK SEM 1");

        jLabel5.setText("IPK SEM 4");

        jLabel6.setText("IPK SEM 3");

        jLabel7.setText("IPK SEM 5");

        Txtsem1.setEditable(false);

        TxtNama.setEditable(false);

        Txtsem2.setEditable(false);

        Txtsem3.setEditable(false);

        Txtsem4.setEditable(false);

        Txtsem5.setEditable(false);

        table2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Nama", "IPSEM1", "IPSEM2", "IPSEM3", "IPSEM4", "IPSEM5"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table2MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table2);

        jLabel8.setText("NAMA");

        TxtNama2.setEditable(false);

        jLabel9.setText("IPK SEM 1");

        Txtsem6.setEditable(false);

        jLabel10.setText("IPK SEM 2");

        Txtsem7.setEditable(false);

        jLabel11.setText("IPK SEM 3");

        Txtsem8.setEditable(false);

        jLabel12.setText("IPK SEM 4");

        Txtsem9.setEditable(false);

        jLabel13.setText("IPK SEM 5");

        Txtsem10.setEditable(false);

        jLabel14.setText("Distance 1");

        TxtDistance.setEditable(false);

        BtnProses1.setText("Proses");
        BtnProses1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnProses1ActionPerformed(evt);
            }
        });

        jLabel15.setText("Distance 2");

        TxtDistance2.setEditable(false);

        jLabel16.setText("Distance  4");

        jLabel17.setText("Distance 3");

        jLabel18.setText("Distance  5");

        TxtDistance3.setEditable(false);

        TxtDistance4.setEditable(false);

        TxtDistance5.setEditable(false);

        BtnProses2.setText("Proses");
        BtnProses2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnProses2ActionPerformed(evt);
            }
        });

        BtnProses3.setText("Proses");
        BtnProses3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnProses3ActionPerformed(evt);
            }
        });

        BtnProses4.setText("Proses");
        BtnProses4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnProses4ActionPerformed(evt);
            }
        });

        BtnProses5.setText("Proses");
        BtnProses5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnProses5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addGap(18, 18, 18)
                                    .addComponent(Txtsem5))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel6)
                                    .addGap(18, 18, 18)
                                    .addComponent(Txtsem3))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addGap(18, 18, 18)
                                    .addComponent(Txtsem4, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel2))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(TxtNama, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Txtsem1)
                                        .addGap(71, 71, 71))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(Txtsem2, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(186, 186, 186)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel13)
                                    .addGap(18, 18, 18)
                                    .addComponent(Txtsem10))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel11)
                                    .addGap(18, 18, 18)
                                    .addComponent(Txtsem8))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel12)
                                    .addGap(18, 18, 18)
                                    .addComponent(Txtsem9, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addComponent(Txtsem7, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(89, 89, 89)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel16))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(TxtDistance3, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BtnProses3))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(TxtDistance4, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BtnProses4))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(TxtDistance5, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BtnProses5))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(TxtDistance2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BtnProses2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(TxtDistance, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BtnProses1)))
                        .addGap(16, 16, 16))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(TxtNama2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Txtsem6, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(TxtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(Txtsem1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(Txtsem2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(Txtsem3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(Txtsem4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(Txtsem5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(TxtNama2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14)
                            .addComponent(TxtDistance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnProses1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(Txtsem6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15)
                            .addComponent(TxtDistance2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnProses2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(Txtsem7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17)
                            .addComponent(TxtDistance3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnProses3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(Txtsem8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16)
                            .addComponent(TxtDistance4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnProses4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(Txtsem9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18)
                            .addComponent(TxtDistance5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnProses5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(Txtsem10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        // TODO add your handling code here:
        TxtNama.setText(table.getValueAt(table.getSelectedRow(),1).toString());
        Txtsem1.setText(table.getValueAt(table.getSelectedRow(),2).toString());
        Txtsem2.setText(table.getValueAt(table.getSelectedRow(),3).toString());
        Txtsem3.setText(table.getValueAt(table.getSelectedRow(),4).toString());
        Txtsem4.setText(table.getValueAt(table.getSelectedRow(),5).toString());
        Txtsem5.setText(table.getValueAt(table.getSelectedRow(),6).toString());
    }//GEN-LAST:event_tableMouseClicked

    private void table2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table2MouseClicked
        // TODO add your handling code here:
        TxtNama2.setText(table2.getValueAt(table2.getSelectedRow(),1).toString());
        Txtsem6.setText(table2.getValueAt(table2.getSelectedRow(),2).toString());
        Txtsem7.setText(table2.getValueAt(table2.getSelectedRow(),3).toString());
        Txtsem8.setText(table2.getValueAt(table2.getSelectedRow(),4).toString());
        Txtsem9.setText(table2.getValueAt(table2.getSelectedRow(),5).toString());
        Txtsem10.setText(table2.getValueAt(table2.getSelectedRow(),6).toString());
    }//GEN-LAST:event_table2MouseClicked

    private void BtnProses1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnProses1ActionPerformed
        // TODO add your handling code here:
        rumus();
    }//GEN-LAST:event_BtnProses1ActionPerformed

    private void BtnProses2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnProses2ActionPerformed
        // TODO add your handling code here:
        rumus2();
    }//GEN-LAST:event_BtnProses2ActionPerformed

    private void BtnProses3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnProses3ActionPerformed
        // TODO add your handling code here:
        rumus3();
    }//GEN-LAST:event_BtnProses3ActionPerformed

    private void BtnProses4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnProses4ActionPerformed
        // TODO add your handling code here:
        rumus4();
    }//GEN-LAST:event_BtnProses4ActionPerformed

    private void BtnProses5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnProses5ActionPerformed
        // TODO add your handling code here:
        rumus5();
    }//GEN-LAST:event_BtnProses5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(knn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(knn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(knn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(knn.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new knn().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnProses1;
    private javax.swing.JButton BtnProses2;
    private javax.swing.JButton BtnProses3;
    private javax.swing.JButton BtnProses4;
    private javax.swing.JButton BtnProses5;
    private javax.swing.JTextField TxtDistance;
    private javax.swing.JTextField TxtDistance2;
    private javax.swing.JTextField TxtDistance3;
    private javax.swing.JTextField TxtDistance4;
    private javax.swing.JTextField TxtDistance5;
    private javax.swing.JTextField TxtNama;
    private javax.swing.JTextField TxtNama2;
    private javax.swing.JTextField Txtsem1;
    private javax.swing.JTextField Txtsem10;
    private javax.swing.JTextField Txtsem2;
    private javax.swing.JTextField Txtsem3;
    private javax.swing.JTextField Txtsem4;
    private javax.swing.JTextField Txtsem5;
    private javax.swing.JTextField Txtsem6;
    private javax.swing.JTextField Txtsem7;
    private javax.swing.JTextField Txtsem8;
    private javax.swing.JTextField Txtsem9;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable table;
    private javax.swing.JTable table2;
    // End of variables declaration//GEN-END:variables
}
